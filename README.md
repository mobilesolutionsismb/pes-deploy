## Preparing the os on the MicroSD ##
***

#### Requirements ####
* Operating System: [Ubuntu 16.04 LTS](http://releases.ubuntu.com/16.04/ubuntu-16.04.5-desktop-amd64.iso)
* HW Tools: 
    - Lettore di microSD
	- SD >= 2gb
* SW Tools:  
	- tar
	- bzip2
	- rename
	- pv
	
To install the required software tools (if needed), execute the following commands on the Ubuntu machine:
```
$: sudo apt-get update 
$: sudo apt-get install tar bzip2 rename pv
```
	
Uncompress the image, and fix the script execution permissions with the following commands:

```console
$: mkdir PES_DEPLOY
$: cd PES_DEPLOY
$: wget https://bitbucket.org/mobilesolutionsismb/pes-deploy/raw/HEAD/full_image.tar.bz2 
$: tar -xjvf full_image.tar.bz2
$: chmod +x sources/meta-ismb-pes/scripts/create_ext_sd.sh
```
	
Everytime you need to create a new SD, execute the following commands, after inserting the SD card in the reader:

```console
$: sudo MACHINE=imx6ul-var-dart sources/meta-ismb-pes/scripts/create_ext_sd.sh /dev/sdX
```
**WARNING: the X in /dev/sdX must be substituted with the letters that identifies the microSD reader (i.e. /dev/sdb or /dev/sdc).** To determine the identification letter of the microSD, you can execute the command
**ls -l /dev/sd\***  (oppure **ll /dev/sd\*** ) before and after inserting the microSD in the reader, and taking notes about the differences. Additional rows determine the identifier.

![ll_sd](wiki/ll_sd.png)
